import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

export class App {
  constructor() {
    this.startApp();
  }

  static rootElement:HTMLElement = <HTMLElement>document.getElementById('root');
  static loadingElement:HTMLElement = <HTMLElement>document.getElementById('loading-overlay');

  async startApp():Promise<string> {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);
      
      App.rootElement.appendChild(fightersElement);
      return('success');
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
      return('error');
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

// export default App;
