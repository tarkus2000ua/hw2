import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { FightersDetails } from '../interfaces/Fighter';

export function renderArena(selectedFighters:FightersDetails[]):void {
  const root:HTMLElement = <HTMLElement>document.getElementById('root');
  const arena:HTMLElement = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(...selectedFighters).then((winner)=>showWinnerModal(winner));
}

function createArena(selectedFighters:FightersDetails[]):HTMLElement {
  const arena:HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators:HTMLElement = createHealthIndicators(...selectedFighters);
  const fighters:HTMLElement = createFighters(...selectedFighters);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(...fighters:FightersDetails[]):HTMLElement 
function createHealthIndicators(leftFighter:FightersDetails, rightFighter:FightersDetails):HTMLElement {
  const healthIndicators:HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign:HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator:HTMLElement = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator:HTMLElement = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter:FightersDetails, position:string):HTMLElement {
  const { name } = fighter;
  const container:HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName:HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator:HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar:HTMLElement = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(...fighters:FightersDetails[]):HTMLElement
function createFighters(firstFighter:FightersDetails, secondFighter:FightersDetails):HTMLElement {
  const battleField:HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement:HTMLElement = createFighter(firstFighter, 'left');
  const secondFighterElement:HTMLElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter:FightersDetails, position:string):HTMLElement {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
