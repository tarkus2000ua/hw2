import { controls } from '../../constants/controls';
import { FightersDetails } from '../interfaces/Fighter';

let playerOneAttackPressed: boolean = false;
let playerTwoAttackPressed: boolean = false;
let playerOneBlockPressed: boolean = false;
let playerTwoBlockPressed: boolean = false;
let playerOneCriticalHitKeys: boolean[] = [false, false, false];
let playerTwoCriticalHitKeys: boolean[] = [false, false, false];
let isCriticalHitPlayerOneDisabled: boolean = false;
let isCriticalHitPlayerTwoDisabled: boolean = false;

export async function fight(...fighters: FightersDetails[]): Promise<FightersDetails>;
export async function fight(firstFighter: FightersDetails, secondFighter: FightersDetails): Promise<FightersDetails> {
  return new Promise((resolve) => {
    const leftHealthIndicator: HTMLElement = <HTMLElement>document.getElementById('left-fighter-indicator');
    const rightHealthIndicator: HTMLElement = <HTMLElement>document.getElementById('right-fighter-indicator');
    const leftIndicatorRate: number = 100 / firstFighter.health;
    const rightIndicatorRate: number = 100 / secondFighter.health;
    const playerOneCriticalHitDamage: number = 2 * firstFighter.attack;
    const playerTwoCriticalHitDamage: number = 2 * secondFighter.attack;

    window.addEventListener('keydown', (event: KeyboardEvent) => {
      switch (event.code) {
        case controls.PlayerOneAttack: {
          if (!playerOneBlockPressed && !playerTwoBlockPressed && !playerOneAttackPressed) {
            playerOneAttackPressed = true;
            let damage = getDamage(firstFighter, secondFighter);
            dealDamage(secondFighter, damage);
            redrawHealthIndicator(secondFighter, rightHealthIndicator, rightIndicatorRate);

            if (secondFighter.health <= 0) {
              resolve(firstFighter);
            }
          }
          break;
        }
        case controls.PlayerTwoAttack: {
          if (!playerOneBlockPressed && !playerTwoBlockPressed && !playerTwoAttackPressed) {
            playerTwoAttackPressed = true;
            let damage: number = getDamage(secondFighter, firstFighter);
            dealDamage(firstFighter, damage);
            redrawHealthIndicator(firstFighter, leftHealthIndicator, leftIndicatorRate);

            if (firstFighter.health <= 0) {
              resolve(secondFighter);
            }
          }
          break;
        }
        case controls.PlayerOneBlock: {
          playerOneBlockPressed = true;
          break;
        }
        case controls.PlayerTwoBlock: {
          playerTwoBlockPressed = true;
          break;
        }
      }

      refreshCriticalHitKeysStatus(event, controls.PlayerOneCriticalHitCombination, playerOneCriticalHitKeys);
      refreshCriticalHitKeysStatus(event, controls.PlayerTwoCriticalHitCombination, playerTwoCriticalHitKeys);

      if (isCriticalHit(playerOneCriticalHitKeys) && !isCriticalHitPlayerOneDisabled) {
        isCriticalHitPlayerOneDisabled = true;
        dealDamage(secondFighter, playerOneCriticalHitDamage);
        redrawHealthIndicator(secondFighter, rightHealthIndicator, rightIndicatorRate);
        if (secondFighter.health <= 0) {
          resolve(firstFighter);
        }
        setTimeout(() => (isCriticalHitPlayerOneDisabled = false), 10000);
      }

      if (isCriticalHit(playerTwoCriticalHitKeys) && !isCriticalHitPlayerTwoDisabled) {
        isCriticalHitPlayerTwoDisabled = true;
        dealDamage(firstFighter, playerTwoCriticalHitDamage);
        redrawHealthIndicator(firstFighter, leftHealthIndicator, leftIndicatorRate);
        if (firstFighter.health <= 0) {
          resolve(secondFighter);
        }
        setTimeout(() => (isCriticalHitPlayerTwoDisabled = false), 10000);
      }
    });

    window.addEventListener('keyup', (event: KeyboardEvent) => {
      switch (event.code) {
        case controls.PlayerOneAttack: {
          playerOneAttackPressed = false;
          break;
        }
        case controls.PlayerTwoAttack: {
          playerTwoAttackPressed = false;
          break;
        }

        case controls.PlayerOneBlock: {
          playerOneBlockPressed = false;
          break;
        }
        case controls.PlayerTwoBlock: {
          playerTwoBlockPressed = false;
          break;
        }
      }

      refreshCriticalHitKeysStatus(event, controls.PlayerOneCriticalHitCombination, playerOneCriticalHitKeys);
      refreshCriticalHitKeysStatus(event, controls.PlayerTwoCriticalHitCombination, playerTwoCriticalHitKeys);
    });
  });
}

export function getDamage(attacker: FightersDetails, defender: FightersDetails): number {
  const hitPower: number = getHitPower(attacker);
  const blockPower: number = getBlockPower(defender);
  const damage: number = Math.max(0, hitPower - blockPower);
  return damage;
}

export function getHitPower(fighter: FightersDetails): number {
  const max = 2;
  const min = 1;
  const criticalHitChance: number = Math.random() * (max - min) + min;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: FightersDetails): number {
  const max = 2;
  const min = 1;
  const dodgeChance: number = Math.random() * (max - min) + min;
  return fighter.defense * dodgeChance;
}

function dealDamage(fighter: FightersDetails, damage: number): void {
  fighter.health -= damage;
}

function isCriticalHit(hitKeys: boolean[]) {
  return hitKeys.every((x) => x) ? true : false;
}

function redrawHealthIndicator(fighter: FightersDetails, indicator: HTMLElement, indicatorRate: number): void {
  let fighterHealthPercents: number = Math.round(fighter.health * indicatorRate);
  fighterHealthPercents = fighterHealthPercents > 0 ? fighterHealthPercents : 0;
  indicator.style.width = fighterHealthPercents + '%';
}

function refreshCriticalHitKeysStatus(
  event: KeyboardEvent,
  combinationCodes: string[],
  combinationKeysStatus: boolean[]
) {
  for (let [index, item] of combinationCodes.entries()) {
    if (event.code == item) {
      if (combinationKeysStatus[index]) {
        combinationKeysStatus[index] = false;
      } else {
        combinationKeysStatus[index] = true;
      }
    }
  }
}
