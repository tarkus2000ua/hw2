import { FightersDetails } from '../interfaces/Fighter';
import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter:FightersDetails, position:string):HTMLElement {
  const positionClassName:string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterPreviewHead:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___head`,
  });
  fighterElement.appendChild(fighterPreviewHead);

  const fighterPreviewBody:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___body`,
  });
  fighterElement.appendChild(fighterPreviewBody);

  const fighterName:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___name`,
  });
  fighterName.innerHTML = fighter.name;
  fighterPreviewHead.appendChild(fighterName);

  const fighterImg:HTMLElement = createFighterImage(fighter);
  fighterPreviewBody.appendChild(fighterImg);

  const fighterPreviewText:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___text`,
  });
  fighterPreviewBody.appendChild(fighterPreviewText);

  const fighterAttack:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___item`,
  });
  fighterAttack.innerHTML = `attack: ${fighter.attack}`;
  fighterPreviewText.appendChild(fighterAttack);

  const fighterDefense:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___item`,
  });
  fighterDefense.innerHTML = `defense: ${fighter.defense}`;
  fighterPreviewText.appendChild(fighterDefense);

  const fighterHealth:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___item`,
  });
  fighterHealth.innerHTML = `health: ${fighter.health}`;
  fighterPreviewText.appendChild(fighterHealth);

  return fighterElement;
}

export function createFighterImage(fighter:FightersDetails):HTMLElement {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement:HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
