import { FightersDetails } from '../interfaces/Fighter';
import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';

export function createFightersSelector() {
  let selectedFighters:FightersDetails[] = [];

  return async (event:Event, fighterId:string) => {
    const fighter:FightersDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

// const fighterDetailsMap:Map<string,string> = new Map();

export async function getFighterInfo(fighterId:string):Promise<FightersDetails> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  const fighter = await fighterService.getFighterDetails(fighterId);
      // for (let prop in <FightersDetails>fighter){
        // fighterDetailsMap.set(prop,fighter[prop]);
      // }
      return fighter;
  
}

function renderSelectedFighters(selectedFighters:FightersDetails[]) {
  const fightersPreview:HTMLElement = <HTMLElement>document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters:FightersDetails[]) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container:HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image:HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn:string = canStartFight ? '' : 'disabled';
  const fightBtn:HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters:FightersDetails[]) {
  renderArena(selectedFighters);
}
