import { FightersDetails } from '../../interfaces/Fighter';
import { showModal } from './modal'
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter:FightersDetails):void {
  const bodyElement:HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterName:HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___name`,
  });
  fighterName.innerHTML = fighter.name;
  bodyElement.appendChild(fighterName);
  const fighterImg = createFighterImage(fighter);
  bodyElement.appendChild(fighterImg);
  const onClose = () => {window.location.reload(false)};
  
  showModal({ title:'The winner is:', bodyElement, onClose});
}
