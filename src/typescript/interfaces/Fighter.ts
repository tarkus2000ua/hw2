export default interface Fighter {
  _id: string;
  name: string;
  source: string;
}

export interface FightersDetails {
  _id: string;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;
}